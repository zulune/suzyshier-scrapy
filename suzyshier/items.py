# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class SuzyshierItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    title = scrapy.Field()
    price = scrapy.Field()
    color = scrapy.Field()
    sizes = scrapy.Field()
    specs = scrapy.Field()
    description = scrapy.Field()


class WebExclusiveItem(scrapy.Item):
    title = scrapy.Field()
    price = scrapy.Field()
    discount_price = scrapy.Field()
