from scrapy import spider
from suzyshier.items import SuzyshierItem, WebExclusiveItem


class ParseData(spider.Spider):
    name = 'bottoms'
    start_urls = ['https://suzyshier.com']

    def start_requests(self):
        bottoms_category = 'https://suzyshier.com/collections/sz_bottoms_shop-all-bottoms'
        yield spider.Request(url=bottoms_category, callback=self.parse)


    def parse(self, response):

        for item in response.css('div.product-tile-image > a::attr(href)').extract():
            yield spider.Request(url=self.start_urls[0] + item, callback=self.parse_product)

        next_page = response.xpath('//select[@class="pagination-dropdown"]::last').extract_first()
        if next_page:
            yield response.follow(response.request + '?page=' + next_page, self.parse)

    def parse_product(self, response):
        title = response.css('h1.product__header::text').extract_first()
        price = response.css('.product__price::text').extract_first()
        color = response.css('label.color::attr(data-value)').extract()
        sizes = response.css('div.selector-wrapper>div>label.radio-size::attr(data-value)').extract()
        specs = response.css('#toggle-product__specs>ul>li::text').extract()
        description = response.css('div#toggle-product__description::text').extract_first()

        yield SuzyshierItem(
            title=title,
            price=price.replace('\n', '').strip(),
            color=color,
            sizes=sizes,
            specs=specs,
            description=description.strip()
        )


class ParseWebExlusive(spider.Spider):
    name = 'exclusive'

    def start_requests(self):
        web_exclusive = 'https://suzyshier.com/collections/sz_trend_online-exclusives'
        yield spider.Request(url=web_exclusive, callback=self.parse_exclusive)

    def parse_exclusive(self, response):
        for item in response.css('div.featured-collection__info.js-product-tile'):
            title = item.css('h2::text').extract_first()
            price = item.css('span.grid-item-price::text').extract_first()
            if price:
                price = price.replace('\n', '')
            else:
                price = ''
            discount_price = item.css('p.grid-item-sale::text').extract_first()
            if discount_price:
                discount_price = discount_price.replace('\n', '')
            else:
                discount_price = ''
            yield WebExclusiveItem(
                title=title,
                price=price,
                discount_price=discount_price
            )