# Scrapy parser

#### Установка

Скачать репозитрий или сделать git clone
Перейти в директорию проекта и создать окружение

``` 
virtualenv env -p python3 
```

Важно: необходимо версия Python > 3.6
После настройки окружения с нужной версией Python установите все зависемости
```
pip install -r requirements.txt
```

Для запуска парсера выполните команды
```
scrapy crawl -o bottoms bottoms.json -t json
scrapy crawl -o exclusive exclusive.json -t json
```